[![build status](https://gitlab.com/gitlab-org/ci-training-slides/badges/master/build.svg)](https://gitlab.com/gitlab-org/ci-training-slides/commits/master)

# CI/CD GitLab Training

This project hosts the RevelJS framework and content that renders the CI/CD trainingt slides. Presentation behavior and
functionality is better understood by reading the [reveal.js](https://github.com/hakimel/reveal.js) instructions. 

These slides also have Speaker Notes to guide the instructor, you can access them by pressing `s` on the presenetation's 
browser window.

## Syllabus

Content is design to be served in one session of about three or three and a half hours. 

- What is CI/CD
- What can you do with CI
- Sample app
  - Activate builds
  - Adding a gitlab-ci.yml file
- Configuring and Setup CI
  - Gitlab-ci.yml
  - Jobs
  - Services
  - Stages
  - Only and except
  - When
  - CI Lint
  - Pipelines and builds
  - Artifacts
  - What are Runners
  - Setup and configure your Runner
  
### Sample Application

The slides require using a sample CI application which can be found at https://gitlab.com/gitlab-org/ci-training-sample  

## reveal.js References:
- [Installation](https://github.com/hakimel/reveal.js/#installation): Step-by-step instructions for getting reveal.js 
running on your computer.
- [Browser Support](https://github.com/hakimel/reveal.js/wiki/Browser-Support): Explanation of browser support and 
fallbacks.
- [Speaker Notes](https://github.com/hakimel/reveal.js/#speaker-notes): Plugin to view speaker notes on a different 
browser window.
- [PDF Export](https://github.com/hakimel/reveal.js/#pdf-export): Export slides to a PDF.
