## GitLab CI/CD

- Test result visibility and validation

- Application Lifecycle

- Continuous Delivery: multiple stages, manual deploys, environments, and
variables

- Open source

Note:
- How does the CI/CD functionality that GitLab offers help the team?
- May be useful to get some ideas about what they are building

----------

- CI is fully integrated with GitLab

- Scalable: Tests run distributed on separate machines of which you can add as
many as you need

- Jobs run in parallel on multiple machines

![CI Architecture](https://about.gitlab.com/images/ci/arch-1.jpg)

Note:
- Mention examples such as building binaries, app packing and delivery, deployment, etc
